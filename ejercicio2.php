<?php

/**
 * Funcion que genera una serie de numeros aleatorios
 * @param int $min valor minimo
 * @param int $max valor maximo
 * @param int $num numero de valores a generar
 * @param int[] $salida array donde se almacena la salida
 */
function ejercicio1($min, $max, $num, &$salida) {
    $salida = []; // array vacio en el que meto los valores

    /*
     * bucle para recorrer el array $vector y rellenarlo
     */
    for ($c = 0; $c < $num; $c++) {
        $salida[$c] = mt_rand($min, $max);
    }

}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $vector=[];
        ejercicio1(1, 10, 10, $vector);
        var_dump($vector);
        ?>
    </body>
</html>
