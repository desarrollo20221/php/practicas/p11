<?php
/**
 * Funcion que genera colores
 * @param int $num numero de colores a generar
 * @return array los colores en un array de cadenas
 */
    function generaColores($num) {
        $colores = [];
        
        for ($c = 0; $c < $num; $c++) {
            $colores[$c]="#";
            for ($j = 0; $j < 7; $j++) {
                $colores[$c].= dechex(mt_rand(0,15));
            }
        }
        return  $colores;
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $colores= generaColores(10);
        var_dump($colores);
        ?>
    </body>
</html>
