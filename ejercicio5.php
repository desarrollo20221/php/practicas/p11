<?php
    /**
     * Funcion que devuelve el numero de veces que se repite los valores del array que pasamos
     * @param int[] $array valores a introducir
     * @param bool $devolverTodos
     * @return int[] array con la repeticion de los valores
     */
    function elementosRepetidos($array,$devolverTodos=false) {
        // variable llave
        $repeated = [];
        
        // recorro el array 
        foreach ((array) $array as $value) {
            $inArray= false;
            
            foreach ($repeated as $i => $rItem) {
                if($rItem['value']===$value){
                    $inArray = true;
                    ++$repeated[$i]["count"];
                    break;
                }
            }
           
            if(false=== $inArray){
                /*$i = count($repeated);
                $repeated[$i] = [];
                $repeated[$i]["value"] = $value;
                $repeated[$i]["count"] = 1;
                 * 
                 */
                
                array_push($repeated,[
                    "value" => $value,
                    "count" => 1
                ]);
                
            }
        }
        // en caso de que el segundo parametro que introduces sea true
        // me elimine los valores que no se repiten
        if(!$devolverTodos){
            foreach ($repeated as $i => $rItem) {
                if($rItem["count"] === 1){
                    unset($repeated[$i]);
                }
            }
        }
        // ordeno el array
        sort($repeated);
        
        return $repeated;
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        var_dump(elementosRepetidos([4,3,1,2,1,1,1,2,3,2],false));
        ?>
    </body>
</html>
