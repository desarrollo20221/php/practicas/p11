<?php
/**
 * Funcion que genera numeros aleatorios
 * @param int $min Valor minimo
 * @param int $max Valor maximo
 * @param int $num Numero de valores que se genera
 * @return int[] conjunto de numeros solicitado
 */
    function ejercicio1($min,$max,$num) {
        $vector = []; // array vacio en el que meto los valores
        
        /*
         * bucle para recorrer el array $vector
         */
        for ($c = 0; $c < $num; $c++) {
            $vector[$c] = mt_rand($min,$max);
        }
        
        // devuelvo el array 
        return $vector;
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $salida = ejercicio1(1, 10, 10);
            var_dump($salida);
        ?>
    </body>
</html>
