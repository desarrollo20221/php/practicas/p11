<?php
    /**
     * 
     * @param type $array
     * @param bool $devolverTodos
     * @return int[]
     */
    function elementosRepetidos($array,$devolverTodos=false) {
        $repeated = [];
        
        foreach ($array as $value) {
            $inArray= false;
            
            foreach ($repeated as $i => $rItem) {
                if($i==$value){
                    $inArray = true;
                    ++$repeated[$i];
                    break;
                }
            }
            if(false=== $inArray){
                $repeated[$value]=1;
                
            }
        }
        
        if(!$devolverTodos){
            foreach ($repeated as $i => $rItem) {
                if($rItem === 1){
                    unset($repeated[$i]);
                }
            }
        }
        ksort($repeated);
        
        return $repeated;
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $vector=[4,3,1,2,1,1,1,2,3,2];
        var_dump(elementosRepetidos($vector,true));
        
        $repeticiones=array_count_values($vector);
        ksort($repeticiones);
        var_dump($repeticiones);
        ?>
    </body>
</html>
