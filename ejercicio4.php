<?php
/**
 * Funcion que genera colores
 * @param int $num numero de colores a generar
 * @param bool $almo=true con valor true nos indica que coloquemos la almohadilla
 * @return array los colores en un array de cadenas
 */
    function generaColores($num, $almo=true) {
        $colores = [];
        
        for ($c = 0; $c < $num; $c++) {
            $a=0;
            $limite=6;
            $colores[$c]="";
            if($almo){
                $colores[$c]="#";
                $limite=7;
            }
            for ($j = 0; $j < 7; $j++) {
                $colores[$c].= dechex(mt_rand(0,15));
            }
            
        }
        return  $colores;
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $colores= generaColores(10,false);
        var_dump($colores);
        ?>
    </body>
</html>
